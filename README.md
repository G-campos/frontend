# Le/tip : APP de gorjetos

### API de cotação de mudas usada:
[AwesomeAPI](https://docs.awesomeapi.com.br/api-de-moedas)

### Motivo:
- Documentação objetiva
- Simplicidade de uso
- Não requer cadastro para uso
- Atualização constante

## Setup do projeto
```
npm install
```

### Compilar e recarregar para desenvolvimento
```
npm run serve
```

### Compilar e minizar para produção
```
npm run build
```

### Corrigir e consertar arquivos
```
npm run lint
```

### Configuração customizada
Veja a [configuração de referência](https://cli.vuejs.org/config/).
